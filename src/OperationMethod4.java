import java.math.BigDecimal;

public class OperationMethod4 implements Strategy{

   @Override
   public BigDecimal method1(int appleNum, int caoMei, int mangGuo) {
      //  个数
      BigDecimal bigDecimal1 = new BigDecimal(appleNum);
      BigDecimal bigDecimal2 = new BigDecimal(caoMei);
      BigDecimal bigDecimal3 = new BigDecimal(mangGuo);
      // 单价
      double caomei = 13 * 0.8;
      BigDecimal bigDecimal4 = new BigDecimal(8);
      BigDecimal bigDecimal5 = new BigDecimal(caomei);
      BigDecimal bigDecimal6 = new BigDecimal(20);
      // 每个苹果的总和
      BigDecimal sumApple = bigDecimal1.multiply(bigDecimal4);
      BigDecimal sumCaomei = bigDecimal2.multiply(bigDecimal5);
      BigDecimal summangGuo = bigDecimal3.multiply(bigDecimal6);
      //总和

      BigDecimal add = sumApple.add(sumCaomei);
      BigDecimal result = add.add(summangGuo);

      BigDecimal subtract = new BigDecimal(0);
      if (result.doubleValue()>100){
         BigDecimal bigDecimal = new BigDecimal(10);
         subtract = result.subtract(bigDecimal);
      }
      return subtract;
   }
}