import java.math.BigDecimal;

public class OperationMethod1 implements Strategy{

   @Override
   public BigDecimal method1(int appleNum, int caoMei, int mangGuo) {
      BigDecimal bigDecimal1 = new BigDecimal(appleNum);
      BigDecimal bigDecimal2 = new BigDecimal(caoMei);
      BigDecimal bigDecimal3 = new BigDecimal(8);
      BigDecimal bigDecimal4 = new BigDecimal(13);

      BigDecimal sumApple = bigDecimal1.multiply(bigDecimal3);
      BigDecimal sumCaomei = bigDecimal2.multiply(bigDecimal4);
      return sumApple.add(sumCaomei);
   }
}