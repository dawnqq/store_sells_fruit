import java.math.BigDecimal;

public class Context {
   private Strategy strategy;
 
   public Context(Strategy strategy){
      this.strategy = strategy;
   }
 
   public BigDecimal executeStrategy(int num1, int num2, int num3){
      return strategy.method1(num1,num2,num3);
   }
}