import java.math.BigDecimal;

public interface Strategy {
   BigDecimal method1(int appleNum,int caoMei,int mangGuo);
}