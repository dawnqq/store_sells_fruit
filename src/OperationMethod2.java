import java.math.BigDecimal;

public class OperationMethod2 implements Strategy{

   @Override
   public BigDecimal method1(int appleNum, int caoMei, int mangGuo) {
      //  个数
      BigDecimal bigDecimal1 = new BigDecimal(appleNum);
      BigDecimal bigDecimal2 = new BigDecimal(caoMei);
      BigDecimal bigDecimal3 = new BigDecimal(mangGuo);
      // 单价
      BigDecimal bigDecimal4 = new BigDecimal(8);
      BigDecimal bigDecimal5 = new BigDecimal(13);
      BigDecimal bigDecimal6 = new BigDecimal(20);
      // 总和
      BigDecimal sumApple = bigDecimal1.multiply(bigDecimal4);
      BigDecimal sumCaomei = bigDecimal2.multiply(bigDecimal5);
      BigDecimal summangGuo = bigDecimal3.multiply(bigDecimal6);

      return sumApple.add(sumCaomei).add(summangGuo);
   }
}