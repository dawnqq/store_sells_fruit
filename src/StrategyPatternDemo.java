public class StrategyPatternDemo {
   public static void main(String[] args) {
      Context context = new Context(new OperationMethod1());
      System.out.println("优惠活动1 " + context.executeStrategy(10,10,0));
 
      context = new Context(new OperationMethod2());
      System.out.println("优惠活动2 " + context.executeStrategy(10, 10,10));
 
      context = new Context(new OperationMethod3());
      System.out.println("优惠活动3 " + context.executeStrategy(10, 10,10));

      context =new Context(new OperationMethod4());
      System.out.println("优惠活动4 "+context.executeStrategy(10, 10, 10));
   }
}